import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'primer-componente',
  templateUrl: './primer-componente.component.html',
  styleUrls: ['./primer-componente.component.css']
})
export class PrimerComponenteComponent implements OnInit {

  edad: number;
  nombres: Array<string>;
  personas: Array<object>;

  constructor() {
    this.edad = 17;
    this.nombres = ['PEdro', 'Luis', 'Angel', 'Fadul'];
    this.personas = [{name: 'Pedro', age: 28, job: 'AgileThought'}];
  }

  ngOnInit(): void {
  }

  aumetarEdad() {
    this.edad++;
    console.log('Aumento');
  }

  quitarEdad() {
    this.edad--;
    console.log('quito');
  }

}
